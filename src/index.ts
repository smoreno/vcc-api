import compression from 'compression';
import cors from 'cors';
import express, { Application } from 'express';
import morgan from 'morgan';
import 'reflect-metadata';
import { createConnection } from 'typeorm';

// Configs
import corsOptions from './core/configs/cors/cors-options';
import { checkEnvironment } from './core/middlewares/check-environment';

// Routes
import administratorRoutes from './api/administrators/administrator.routes';
import authRoutes from './api/auth/auth.routes';
import categoryRoutes from './api/categories/category.routes';
import clientRoutes from './api/clients/client.routes';
import driverRoutes from './api/drivers/driver.routes';
import indexRoutes from './api/index/index.routes';
import orderRoutes from './api/orders/order.routes';
import subcategoryRoutes from './api/subcategories/subcategory.routes';

/**
 * Server main of the application
 */
class Server {
  public app: Application;
  private pathFiles = {
    avatars: '/uploads/avatars',
    logos: '/uploads/logos',
  };

  constructor() {
    this.app = express();
    this.vars();
    this.config();
    this.routes();
    this.database();
  }

  public vars(): void {
    // Loading vars
    const env = checkEnvironment();

    // Settings variables
    console.log('Environment on', env.ENVIRONMENT);
    this.app.set('NODE_PORT', env.NODE_PORT);
    this.app.set('NODE_SERVER', env.NODE_SERVER);
    this.app.set('DB_PORT', env.DB_PORT);
    this.app.set('DB_HOST', env.DB_HOST);
    this.app.set('DB_NAME', env.DB_NAME);
    this.app.set('DB_USER', env.DB_USER);
    this.app.set('DB_PASSWORD', env.DB_PASSWORD);
    this.app.set('DB_CHARSET', env.DB_CHARSET);
    this.app.set('DB_SYNCHRONIZE', env.DB_SYNCHRONIZE);
    this.app.set('DB_LOGGING', env.DB_LOGGING);
  }

  public config(): void {
    this.app.use(morgan('dev')); // View HTTP requests by terminal
    this.app.use(express.json({limit: '1024mb'})); // Accept JSON data on the request and response
    this.app.use(compression()); // Enabled compression
    this.app.use(cors(corsOptions)); // Enabled CORS for another servers
    this.app.use(express.urlencoded({extended: true})); // Accept HTML forms requests

    // Public folders
    this.app.use(this.pathFiles.avatars, express.static(__dirname + this.pathFiles.avatars));
    this.app.use(this.pathFiles.logos, express.static(__dirname + this.pathFiles.logos));
  }

  public routes(): void {
    this.app.use('/', cors(corsOptions), indexRoutes);
    this.app.use('/auth', cors(corsOptions), authRoutes);
    this.app.use('/category', cors(corsOptions), categoryRoutes);
    this.app.use('/subcategory', cors(corsOptions), subcategoryRoutes);
    this.app.use('/administrator', cors(corsOptions), administratorRoutes);
    this.app.use('/client', cors(corsOptions), clientRoutes);
    this.app.use('/driver', cors(corsOptions), driverRoutes);
    this.app.use('/order', cors(corsOptions), orderRoutes);
  }

  public database(): void {
    console.log('Connecting to database');

    // Create connection to database
    createConnection({
      type: 'mysql',
      port: this.app.get('DB_PORT'),
      host: this.app.get('DB_HOST'),
      database: this.app.get('DB_NAME'),
      username: this.app.get('DB_USER'),
      password: this.app.get('DB_PASSWORD'),
      charset: this.app.get('DB_CHARSET'),
      synchronize: this.app.get('DB_SYNCHRONIZE'),
      logging: this.app.get('DB_LOGGING'),
      entities: [ 'dist/core/database/entities/**/*.js' ],
      migrations: [ 'dist/core/database/migrations/**/*.js' ],
      subscribers: [ 'dist/core/database/subscribers/**/*.js' ],
      cli: {
        entitiesDir: 'src/core/database/entities',
        migrationsDir: 'src/core/database/migrations',
        subscribersDir: 'src/core/database/subscribers',
      },
    })
      .then(() => console.log('Database synchronization success'))
      .catch((error: any) => console.log('Error on connection to database', error));
  }

  public start(): void {
    this.app.listen(process.env.PORT || this.app.get('NODE_PORT'), () => {
      console.log('Server on port ' + this.app.get('NODE_PORT'));
    });
  }
}

// Server instance
const server = new Server();
server.start();

// Exporting the app
export default server.app;
