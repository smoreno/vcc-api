import { Request, Response } from 'express';

class IndexController {
  public async list(req: Request, res: Response) {
    res.status(200).json({ text: 'Works it!' });
  }
}

export const indexController = new IndexController();
