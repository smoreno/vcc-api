import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { authController } from './auth.controller';

class AuthRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/validate/:token', authController.ctrlValidateToken);
    this.router.post('/login', authController.ctrlLogin);
    this.router.post('/request-change-password', authController.ctrlRequestChangePassword);
    this.router.delete('/logout', checkJwt, authController.ctrlLogout);
    this.router.patch('/change-password', checkJwt, authController.ctrlChangePassword);
  }
}

export default new AuthRoutes().router;
