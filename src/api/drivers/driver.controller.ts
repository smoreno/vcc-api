import { validate } from 'class-validator';
import { Request, Response } from 'express';

// Models
import { AdministratorModel } from '../administrators/administrator.model';
import { DriverModel } from './driver.model';

// Entities
import { Driver } from '../../core/database/entities/driver';
import { User } from '../../core/database/entities/user';

class DriverController {
  /**
   * Loads all drivers from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const dv = new DriverModel();
      const users = await dv.list();
      res.status(200).json({ code: 200, response: users });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load driver by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find Driver
      const dv = new DriverModel();
      const user: Driver = await dv.getOne(parseInt(req.params.id));

      // Valid the info
      if (user) {
        res.status(200).json({ code: 200, response: user });
      } else {
        res.status(404).json({ code: 404, response: 'Paciente no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a driver on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const dv = new DriverModel();

      // Creating a instance of User
      const user = new User();
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.email = req.body.email;
      user.role = 'Driver';

      // Validate data User
      const errorsUser = await validate(user);
      if (errorsUser.length > 0) {
        res.status(400).json({ code: 400, response: errorsUser });
        return;
      }

      // Return data
      const dataUser: User = await am.saveChanges(user);

      // Personal and contact information
      // Creating a instance of Driver
      const driver = new Driver();
      driver.phone = req.body.phone;
      driver.user = dataUser;

      // Validate data Driver
      const errorsDriver = await validate(user);
      if (errorsDriver.length > 0) {
        res.status(400).json({ code: 400, response: errorsDriver });
        return;
      }

      // Return data
      const result: Driver = await dv.saveChanges(driver);
      res.status(200).json({ code: 200, response: result });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a driver on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const dv = new DriverModel();

      // Creating a instance of driver
      const driver: Driver = await dv.getOne(req.body.id);
      driver.phone = req.body.phone;

      // Validate data driver
      const errorsDriver = await validate(driver);
      if (errorsDriver.length > 0) {
        res.status(400).json({ code: 400, response: errorsDriver });
        return;
      }

      const dataDriver: Driver = await dv.saveChanges(driver);

      if (dataDriver.user) {
        // Creating a instance of User
        driver.user.firstName = req.body.firstName;
        driver.user.lastName = req.body.lastName;
        driver.user.email = req.body.email;

        // Validate data User
        const errorsUser = await validate(driver.user);
        if (errorsUser.length > 0) {
          res.status(400).json({ code: 400, response: errorsUser });
          return;
        }

        // Return data
        const result: User = await am.saveChanges(driver.user);

        res.status(200).json({ code: 200, response: result });
      } else {
        res.status(400).json({ code: 400, response: 'Fallo al actualizar los datos del usuario' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a driver on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Driver
      const am = new AdministratorModel();
      const dv = new DriverModel();
      const driver = await dv.getOne(parseInt(req.params.id));
      if (driver) {
        // Return data
        const data = await am.remove(driver.user.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Driver not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const driverController = new DriverController();
