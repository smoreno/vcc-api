import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { driverController } from './driver.controller';

class DriverRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, driverController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, driverController.ctrlGetOne);
    this.router.post('/', checkJwt, driverController.ctrlCreate);
    this.router.patch('/', checkJwt, driverController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, driverController.ctrlRemove);
  }
}
export default new DriverRoutes().router;
