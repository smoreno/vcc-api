import { DeleteResult, getManager } from 'typeorm';
import { Driver } from '../../core/database/entities/driver';

export class DriverModel {
  public async list(): Promise<Driver[]> {
    //  Get repository to operations
    const driverRepository = getManager().getRepository(Driver);

    // Return the list of drivers
    return await driverRepository.find({
      relations: [ 'user'],
      where: { role: 'Driver' },
    });
  }

  public async getOne(driverId: number): Promise<Driver> {
    //  Get repository to operations
    const driverRepository = getManager().getRepository(Driver);

    // Return driver by id
    return await driverRepository.findOne({
      relations: [ 'user' ],
      where: { id: driverId},
    });
  }

  public async saveChanges(driver: Driver): Promise<Driver> {
    return await getManager().getRepository(Driver).save(driver);
  }

  public async remove(driverId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Driver).delete(driverId);
  }

  public async verifyDocument(driverDocument: string): Promise<Driver> {
    //  Get repository to operations
    const driverRepository = getManager().getRepository(Driver);

    // Return user by email and password
    return await driverRepository.findOne({
      relations: [ 'user' ],
      where: { documentNumber: driverDocument },
     });
  }
}
