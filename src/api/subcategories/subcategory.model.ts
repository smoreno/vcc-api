import { DeleteResult, getManager } from 'typeorm';
import { Subcategory } from '../../core/database/entities/subcategory';

export class SubcategoryModel {
  public async getOne(subcategoryId: number): Promise<Subcategory> {
    return await getManager().getRepository(Subcategory).findOne({
      relations: ['category'],
      where: { id: subcategoryId },
    });
  }

  public async saveChanges(subcategory: Subcategory): Promise<Subcategory> {
    return await getManager().getRepository(Subcategory).save(subcategory);
  }

  public async remove(subcategoryId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Subcategory).delete(subcategoryId);
  }
}
