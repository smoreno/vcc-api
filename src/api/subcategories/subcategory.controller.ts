import { validate } from 'class-validator';
import { Request, Response } from 'express';
import { Subcategory } from '../../core/database/entities/subcategory';
import { getCleanedString } from '../../core/helpers/Utils';
import { CategoryModel } from '../categories/category.model';
import { SubcategoryModel } from './subcategory.model';

class SubcategoryController {
  /**
   * Create a subcategory on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const scm = new SubcategoryModel();
      const cm = new CategoryModel();
      const subcategory = new Subcategory();



      // Search category
      const category = await cm.getOne(req.body.catId);
      if (category) {
        // Validate editable field
        if (category.editable) {
          // Set object
          const variable = getCleanedString(req.body.name);

          const categorySubcateries = await cm.getOneWithSubCByName(category.name);
          console.log(categorySubcateries);


          subcategory.name = req.body.name;
          subcategory.category = category;

          // Validate object
          const errors = await validate(subcategory);
          if (errors.length > 0) {
            res.status(400).json({ code: 400, response: errors });
          }

          // Save
          const data = await scm.saveChanges(subcategory);
          res.status(200).json({ code: 200, response: data });
        } else {
          res.status(400).json({ code: 400, response: 'Subcategoría no editable' });
          return;
        }
      } else {
        res.status(404).json({ code: 404, response: 'Categoría no encontrada' });
        return;
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a subcategory on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      // Search subcategory
      const scm = new SubcategoryModel();
      const sc: Subcategory = await scm.getOne(parseInt(req.body.id));

      if (sc) {
        // Validate editable field
        if (sc.category.editable) {
          // Set object
          sc.name = req.body.name;

          // Validate object
          const errors = await validate(sc);
          if (errors.length > 0) {
            res.status(400).json({ code: 400, response: errors });
          }

          // Save
          const data = await scm.saveChanges(sc);
          res.status(200).json({ code: 200, response: data });
        } else {
          res.status(400).json({ code: 400, response: 'Subcategoría no editable' });
          return;
        }
      } else {
        res.status(404).json({ code: 404, response: 'Subcategoría no encontrada' });
        return;
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a subcategory on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search subcategory
      const scm = new SubcategoryModel();
      const sc = await scm.getOne(parseInt(req.params.id));
      if (sc) {
        // Validate editable field
        if (sc.category.editable) {
          const data = await scm.remove(parseInt(req.params.id));
          res.status(200).json({ code: 200, response: data });
        } else {
          res.status(401).json({ code: 401, response: 'Subcategoría no editable' });
          return;
        }
      } else {
        res.status(404).json({ code: 404, response: 'Subcategoría no encontrada' });
        return;
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const subcategoryController = new SubcategoryController();
