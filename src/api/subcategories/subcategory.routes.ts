import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { subcategoryController } from './subcategory.controller';

class SubcategoryRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.post('/', checkJwt, subcategoryController.ctrlCreate);
    this.router.put('/', checkJwt, subcategoryController.ctrlUpdate);
    this.router.delete('/:id([0-9]+)', checkJwt, subcategoryController.ctrlRemove);
  }
}

export default new SubcategoryRoutes().router;
