import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { orderCheckInController } from './order-check-in.controller';

class OrderCheckInRoutes {
  public router: Router = Router();
}

export default new OrderCheckInRoutes().router;
