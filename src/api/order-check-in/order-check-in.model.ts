import { DeleteResult, getManager } from 'typeorm';
import { OrderCheckIn } from '../../core/database/entities/order-check-in';

export class OrderCheckInModel {
  public async list(): Promise<OrderCheckIn[]> {
    return await getManager().getRepository(OrderCheckIn).find({
      relations: [ 'patient', 'patient.user' ],
    });
  }

  public async getOne(requestId: number): Promise<OrderCheckIn> {
    return await getManager().getRepository(OrderCheckIn).findOne({
      relations: [ 'patient', 'patient.user' ],
      where: { patient: { id: requestId } },
    });
  }

  public async getOneByUpdate(requestId: number): Promise<OrderCheckIn> {
    return await getManager().getRepository(OrderCheckIn).findOne({
      where: { Patientid: requestId },
    });
  }

  public async getList(requestId: number): Promise<OrderCheckIn[]> {
    return await getManager().getRepository(OrderCheckIn).find({
      relations: [ 'patient', 'patient.user' ],
      where: { patient: { id: requestId } },
    });
  }

  public async getOneForDel(orderCheckInId: number): Promise<OrderCheckIn> {
    return await getManager().getRepository(OrderCheckIn).findOne({
      where: { id: orderCheckInId },
    });
  }

  public async saveChanges(orderCheckIn: OrderCheckIn): Promise<OrderCheckIn> {
    return await getManager().getRepository(OrderCheckIn).save(orderCheckIn);
  }

  public async remove(orderCheckInId: number): Promise<DeleteResult> {
    return await getManager().getRepository(OrderCheckIn).delete(orderCheckInId);
  }
}
