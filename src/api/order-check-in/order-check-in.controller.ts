import { validate } from 'class-validator';
import { Request, Response } from 'express';

// MODELS
import { AdministratorModel } from '../administrators/administrator.model';
import { OrderCheckInModel } from './order-check-in.model';

// ENTITIES
import { OrderCheckIn } from '../../core/database/entities/order-check-in';
import { User } from '../../core/database/entities/user';

class OrderCheckInController {
  /**
   * Loads all the check in from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const mq = new OrderCheckInModel();
      const queries = await mq.list();
      res.status(200).json({ code: 200, response: queries });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load check in by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find Check in
      const mq   = new OrderCheckInModel();
      const query: OrderCheckIn = await mq.getOne(parseInt(req.params.id));

      // Valid the info
      if (query) {
        res.status(200).json({ code: 200, response: query });
      } else {
        res.status(404).json({ code: 404, response: 'Check In no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Loads all the Check in by a patient from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetList(req: Request, res: Response): Promise<void> {
    try {
      const mq = new OrderCheckInModel();
      const queries = await mq.getList(parseInt(req.params.id));
      res.status(200).json({ code: 200, response: queries });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a patient on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const mq = new OrderCheckInModel();

        // Creating a instance of Medical Query
      const orderCheckIn = new OrderCheckIn();
      orderCheckIn.alzavidrio = req.body.alzavidrio;

      const result: OrderCheckIn = await mq.saveChanges(orderCheckIn);
      // const dataMedicalQuery: MedicalQuery = await mq.saveChanges(medicalQuery);

      // const result = (dataMedicalQuery !== null) ? 'Datos guardados con exito' : 'Ocurrio un error inesperado';
      res.status(200).json({ code: 200, response: result });

      // Creating User
      //const dataUser: User = await am.saveChanges(user);
      res.status(200).json({ code: 200, response: result });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a doctor on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const oc = new OrderCheckInModel();
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a patient on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Doctor
      const mq = new OrderCheckInModel();
      const query = await mq.getOneForDel(parseInt(req.params.id));
      if (query) {
        // Return data
        const data = await mq.remove(query.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Query not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const orderCheckInController = new OrderCheckInController();
