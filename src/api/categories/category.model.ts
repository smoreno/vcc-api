import { getManager } from 'typeorm';
import { Category } from '../../core/database/entities/category';

export class CategoryModel {
  public async list(): Promise<Category[]> {
    //  Get repository to operations
    const categoryRepository = getManager().getRepository(Category);

    // Return the list of categorys
    return await categoryRepository.find({ relations: ['subcategories'] });
  }

  public async getOneWithSubcategories(catId: number): Promise<Category> {
    //  Get repository to operations
    const categoryRepository = getManager().getRepository(Category);

    // Return category by id
    return await categoryRepository.findOne({
      relations: ['subcategories'],
      where: { id: catId },
    });
  }

  public async getOneWithSubCByName(catName: string): Promise<Category> {
    //  Get repository to operations
    const categoryRepository = getManager().getRepository(Category);

    // Return category by name
    return await categoryRepository.findOne({
      relations: ['subcategories'],
      where: { name: catName },
    });
  }

  public async getOne(catId: number): Promise<Category> {
    //  Get repository to operations
    const categoryRepository = getManager().getRepository(Category);

    // Return category by id
    return await categoryRepository.findOne(catId);
  }
}
