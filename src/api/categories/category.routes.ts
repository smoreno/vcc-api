import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { categoryController } from './category.controller';

class CategoryRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, categoryController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, categoryController.ctrlGetOneWithSubcategories);
  }
}

export default new CategoryRoutes().router;
