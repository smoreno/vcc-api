import { Request, Response } from 'express';
import { CategoryModel } from './category.model';

class CategoryController {
  /**
   * Loads all categorys from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const dm = new CategoryModel();
      const data = await dm.list();
      res.status(200).json({ code: 200, response: data });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load category by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOneWithSubcategories(req: Request, res: Response): Promise<void> {
    try {
      const dm = new CategoryModel();
      const data = await dm.getOneWithSubcategories(parseInt(req.params.id));
      if (data) {
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(400).json({ code: 400, response: 'El Id de la Categoría inválido' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const categoryController = new CategoryController();
