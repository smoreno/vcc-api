import { validate } from 'class-validator';
import { Request, Response } from 'express';
import { Client } from '../../core/database/entities/client';
import { User } from '../../core/database/entities/user';
import { AdministratorModel } from '../administrators/administrator.model';
import { ClientModel } from './client.model';

class ClientController {
  /**
   * Loads all clients from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const cl = new ClientModel();
      const users = await cl.list();
      res.status(200).json({ code: 200, response: users });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load client by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find Client
      const cl = new ClientModel();
      const user: Client = await cl.getOne(parseInt(req.params.id));

      // Valid the info
      if (user) {
        res.status(200).json({ code: 200, response: user });
      } else {
        res.status(404).json({ code: 404, response: 'CLient no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a client on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const cl = new ClientModel();

      // Creating a instance of User
      const user = new User();
      user.firstName = req.body.firstName;
      user.lastName = req.body.lastName;
      user.email = req.body.email;
      user.email = user.email.toLowerCase();
      user.photo = '';
      user.password = req.body.password;
      user.hashPassword();
      user.role = 'CNatural';

      // Validate data User
      const errorsUser = await validate(user);
      if (errorsUser.length > 0) {
        res.status(400).json({ code: 400, response: errorsUser });
        return;
      }

      // Return data
      const dataUser: User = await am.saveChanges(user);

      // Creating a instance of CLient
      const client = new Client();
      client.ine = req.body.ine;
      client.gender = req.body.gender;
      client.address = req.body.address;
      client.phone = req.body.phone;
      client.user = dataUser;

      // Validate data client
      const errorsCLient = await validate(user);
      if (errorsCLient.length > 0) {
        res.status(400).json({ code: 400, response: 'Correo electrónico ya registrado en nuestra plataforma' });
        return;
      }

      // Return data
      const dataCLient: Client = await cl.saveChanges(client);
      res.status(200).json({ code: 200, response: dataCLient });
    } catch (error) {
      res.status(500).json({ code: 500, response: 'Correo electrónico ya registrado en nuestra plataforma' });
    }
  }

  /**
   * Create a client on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const cl = new ClientModel();

      // Creating a instance of Client
      const client: Client = await cl.getOne(req.body.id);
      client.ine = req.body.ine;
      client.gender = req.body.gender;
      client.address = req.body.address;
      client.phone = req.body.phone;

      // Validate data Client
      const errorsClient = await validate(client);
      if (errorsClient.length > 0) {
        res.status(400).json({ code: 400, response: errorsClient });
        return;
      }

      // Return data
      const dataClient: Client = await cl.saveChanges(client);

      if (dataClient.user) {
        // Creating a instance of User
        client.user.firstName = req.body.firstName;
        client.user.lastName = req.body.lastName;
        client.user.email = req.body.email;
        client.user.email = client.user.email.toLowerCase();

        // Validate data User
        const errorsUser = await validate(client.user);
        if (errorsUser.length > 0) {
          res.status(400).json({ code: 400, response: errorsUser });
          return;
        }

        // Return data
        const dataUser: User = await am.saveChanges(client.user);
        res.status(200).json({ code: 200, response: dataUser });
      } else {
        res.status(400).json({ code: 400, response: 'Fallo al actualizar los datos del usuario' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a client notifications on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdateNotifications(req: Request, res: Response): Promise<void> {
    try {
      // Searching the client to update
      const cl = new ClientModel();
      const client: Client = await cl.getOne(req.body.id);

      // Updating the fields
      // client.newAppointmentCreated = req.body.newAppointmentCreated;
      // client.appointmentReminder = req.body.appointmentReminder;
      // client.paymentCompleted = req.body.paymentCompleted;

      // Validate instance
      const errors = await validate(client);
      if (errors.length > 0) {
        res.status(400).json({ code: 400, response: errors });
        return;
      }

      // Return data
      const dataclient: Client = await cl.updateNotifications(client);
      res.status(200).json({ code: 200, response: dataclient });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Update a client profile on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdateProfile(req: Request, res: Response): Promise<void> {
    try {
      const am = new AdministratorModel();
      const cl = new ClientModel();

      // Creating a instance of Client
      const client: Client = await cl.getOne(req.body.id);
      client.phone = req.body.phone;

      // Validate data Client
      const errorsClient = await validate(client);
      if (errorsClient.length > 0) {
        res.status(400).json({ code: 400, response: errorsClient });
        return;
      }

      // Return data
      const dataClient: Client = await cl.saveChanges(client);

      if (dataClient.user) {
        // Creating a instance of User
        client.user.email = req.body.email;
        client.user.email = client.user.email.toLowerCase();

        // Validate data User
        const errorsUser = await validate(client.user);
        if (errorsUser.length > 0) {
          res.status(400).json({ code: 400, response: errorsUser });
          return;
        }

        // Return data
        const dataUser: User = await am.saveChanges(client.user);
        res.status(200).json({ code: 200, response: dataUser });
      } else {
        res.status(400).json({ code: 400, response: 'Fallo al actualizar los datos del usuario' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Remove a client on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    try {
      // Search Client
      const am = new AdministratorModel();
      const cl = new ClientModel();
      const client = await cl.getOne(parseInt(req.params.id));
      if (client) {
        // Return data
        const data = await am.remove(client.user.id);
        res.status(200).json({ code: 200, response: data });
      } else {
        res.status(404).json({ code: 404, response: 'Client not found' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }
}

export const clientController = new ClientController();
