import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { clientController } from './client.controller';

class ClientRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    this.router.get('/', checkJwt, clientController.ctrlList);
    this.router.get('/:id([0-9]+)', checkJwt, clientController.ctrlGetOne);
    this.router.post('/',  clientController.ctrlCreate);
    this.router.put('/', checkJwt, clientController.ctrlUpdate);
    this.router.patch('/update-notifications', checkJwt, clientController.ctrlUpdateNotifications);
    this.router.patch('/update-profile', checkJwt, clientController.ctrlUpdateProfile);
    this.router.delete('/:id([0-9]+)', checkJwt, clientController.ctrlRemove);
  }
}

export default new ClientRoutes().router;
