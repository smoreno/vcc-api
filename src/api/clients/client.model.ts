import { DeleteResult, getManager } from 'typeorm';
import { Client } from '../../core/database/entities/client';

export class ClientModel {
  public async list(): Promise<Client[]> {
    //  Get repository to operations
    const clientRepository = getManager().getRepository(Client);

    // Return the list of customers
    return await clientRepository.find({
      relations: [ 'user' ],
      where: { role: 'Client' },
    });
  }

  public async getOne(clientId: number): Promise<Client> {
    return await getManager().getRepository(Client).findOne({
      relations: [ 'user' ],
      where: { id: clientId, role: 'Client' },
    });
  }

  public async getOneByUserId(userId: number): Promise<Client> {
     return await getManager().getRepository(Client).findOne({
       relations: [ 'user' ],
       where: { user: { id: userId }, role: 'Client' },
    });
  }

  public async saveChanges(doctor: Client): Promise<Client> {
    return await getManager().getRepository(Client).save(doctor);
  }

  public async updateNotifications(client: Client): Promise<Client> {
    //  Get repository to operations
    const clientRepository = getManager().getRepository(Client);

    // Return client updated
    return await clientRepository.save(client);
  }

  public async remove(clientId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Client).delete(clientId);
  }
}
