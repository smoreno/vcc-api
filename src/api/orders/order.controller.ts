import { validate } from 'class-validator';
import { Request, Response } from 'express';

// MODELS
import { AdministratorModel } from '../administrators/administrator.model';
import { ClientModel } from '../clients/client.model';
import { OrderModel } from './order.model';

// ENTITIES
import { Client } from '../../core/database/entities/client';
import { Order } from '../../core/database/entities/order';
import { User } from '../../core/database/entities/user';

class OrderController {
  /**
   * Loads all the queries from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlList(req: Request, res: Response): Promise<void> {
    try {
      const or = new OrderModel();
      const orders = await or.list();
      res.status(200).json({ code: 200, response: orders });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Load query by id from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetOne(req: Request, res: Response): Promise<void> {
    try {
      // Find Order
      const or   = new OrderModel();
      const order: Order = await or.getOne(parseInt(req.params.id));

      // Valid the info
      if (order) {
        res.status(200).json({ code: 200, response: order });
      } else {
        res.status(404).json({ code: 404, response: 'Orden no encontrado' });
      }
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Loads all the order by a patient from the database
   * @param req Request
   * @param res Response
   */
  public async ctrlGetList(req: Request, res: Response): Promise<void> {
    try {
      const or = new OrderModel();
      const orders = await or.getList(parseInt(req.params.id));
      res.status(200).json({ code: 200, response: orders });
    } catch (error) {
      res.status(500).json({ code: 500, response: error });
    }
  }

  /**
   * Create a patient on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlCreate(req: Request, res: Response): Promise<void> {
    // try {
    //   const cli = new ClientModel();
    //   const am = new AdministratorModel();
    //   const or = new OrderModel();

    //   // It is verified that the patient already exists in the database
    //   const patientDocument = await cli.verifyDocument(req.body.documentNumber);

    //   if (patientDocument) {
    //     // Order
    //     // Creating a instance of Order
    //     const order = new Order();
    //     order.opinionNose = req.body.opinionNose;
    //     order.frontPicture = req.body.frontPicture;
    //     order.basePicture = req.body.basePicture;
    //     order.profilePicture = req.body.profilePicture;
    //     order.customPicture = req.body.customPicture;
    //     order.patient = patientDocument;

    //     const result: Order = await or.saveChanges(order);
    //     // const dataMedicalQuery: Order = await mq.saveChanges(medicalQuery);

    //     // const result = (dataMedicalQuery !== null) ? 'Datos guardados con exito' : 'Ocurrio un error inesperado';
    //     res.status(200).json({ code: 200, response: result });

    //   } else {
    //     // Creating a instance of User
    //     const user = new User();
    //     user.firstName = req.body.firstName;
    //     user.lastName = req.body.lastName;
    //     user.email = req.body.email;
    //     user.role = 'Paciente';

    //     // Validate data User
    //     const errorsUser = await validate(user);
    //     if (errorsUser.length > 0) {
    //       res.status(400).json({ code: 400, response: errorsUser });
    //       return;
    //     }

    //     // Creating User
    //     const dataUser: User = await am.saveChanges(user);

    //     // Personal and contact information
    //     // Creating a instance of Patient
    //     const patient = new Patient();
    //     patient.documentType = req.body.documentType;
    //     patient.documentNumber = req.body.documentNumber;
    //     patient.gender = req.body.gender;
    //     patient.birthdate = req.body.birthdate;
    //     patient.address = req.body.address;
    //     patient.country = req.body.country;
    //     patient.phone = req.body.phone;
    //     patient.user = dataUser;

    //     // Validate data Patient
    //     const errorsPatient = await validate(user);
    //     if (errorsPatient.length > 0) {
    //       res.status(400).json({ code: 400, response: errorsPatient });
    //       return;
    //     }

    //     // Creating Patient
    //     const dataPatient: Patient = await cli.saveChanges(patient);

    //     // Personal history
    //     // Creating a instance of Medical History
    //     const medicalHistory = new MedicalHistory();
    //     medicalHistory.isPregnant = req.body.isPregnant;
    //     medicalHistory.youSmoke = req.body.youSmoke;
    //     medicalHistory.disease = req.body.disease;
    //     medicalHistory.processSurgical = req.body.processSurgical;
    //     medicalHistory.medication = req.body.medication;
    //     medicalHistory.patient = dataPatient;

    //     // Return data
    //     const dataMedicalHistory: MedicalHistory = await mh.saveChanges(medicalHistory);

    //     // Validate data Medical History
    //     const errorsMedicalHistory = await validate(dataMedicalHistory);
    //     if (errorsPatient.length > 0) {
    //       res.status(400).json({ code: 400, response: errorsMedicalHistory });
    //       return;
    //     }

    //     // Medical Query
    //     // Creating a instance of Medical Query
    //     const Order = new Order();
    //     medicalQuery.origin = true;
    //     medicalQuery.opinionNose = req.body.opinionNose;
    //     medicalQuery.frontPicture = req.body.frontPicture;
    //     medicalQuery.basePicture = req.body.basePicture;
    //     medicalQuery.profilePicture = req.body.profilePicture;
    //     medicalQuery.customPicture = req.body.customPicture;
    //     medicalQuery.patient = dataPatient;

    //     const result: Order = await mq.saveChanges(medicalQuery);
    //     // const dataMedicalQuery: Order = await mq.saveChanges(medicalQuery);

    //     // const result = (dataMedicalQuery !== null) ? 'Datos guardados con exito' : 'Ocurrio un error inesperado';
    //     res.status(200).json({ code: 200, response: result });
    //   }
    // } catch (error) {
    //   res.status(500).json({ code: 500, response: error });
    // }
  }

  /**
   * Create a doctor on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlUpdate(req: Request, res: Response): Promise<void> {
    // try {
    //   const am = new AdministratorModel();
    //   const cli = new ClientModel();
    //   const or = new OrderModel();

    //   // Creating a instance of Patient
    //   const patient: Patient = await cli.getOne(req.body.id);
    //   patient.documentType = req.body.documentType;
    //   patient.documentNumber = req.body.documentNumber;
    //   patient.gender = req.body.gender;
    //   patient.birthdate = req.body.birthdate;
    //   patient.address = req.body.address;
    //   patient.country = req.body.country;
    //   patient.phone = req.body.phone;

    //   // Validate data Patient
    //   const errorsPatient = await validate(patient);
    //   if (errorsPatient.length > 0) {
    //     res.status(400).json({ code: 400, response: errorsPatient });
    //     return;
    //   }

    //   // Return data
    //   const dataPatient: Patient = await cli.saveChanges(patient);

    //   if (dataPatient.user) {
    //     // Creating a instance of User
    //     patient.user.firstName = req.body.firstName;
    //     patient.user.lastName = req.body.lastName;
    //     patient.user.email = req.body.email;
    //     patient.user.email = patient.user.email.toLowerCase();

    //     // Validate data User
    //     const errorsUser = await validate(patient.user);
    //     if (errorsUser.length > 0) {
    //       res.status(400).json({ code: 400, response: errorsUser });
    //       return;
    //     }

    //     // Return data
    //     const dataUser: User = await am.saveChanges(patient.user);

    //     // Return data medical history patient
    //     const medicalHistory: MedicalHistory = await mh.getOneByUpdate(patient.user.id);

    //     // Personal history
    //     // Creating a instance of Medical History
    //     // const medicalHistory = new MedicalHistory();
    //     medicalHistory.isPregnant = req.body.isPregnant;
    //     medicalHistory.youSmoke = req.body.youSmoke;
    //     medicalHistory.disease = req.body.disease;
    //     medicalHistory.processSurgical = req.body.processSurgical;
    //     medicalHistory.medication = req.body.medication;

    //     // Return data
    //     const dataMedicalHistory: MedicalHistory = await mh.saveChanges(medicalHistory);

    //     // Validate data Medical History
    //     const errorsMedicalHistory = await validate(dataMedicalHistory);
    //     if (errorsPatient.length > 0) {
    //       res.status(400).json({ code: 400, response: errorsMedicalHistory });
    //       return;
    //     }

    //     // Return data medical history patient
    //     const medicalQuery: Order = await mq.getOneByUpdate(patient.id);

    //     // Medical Query
    //     // Creating a instance of Medical Query
    //     // const Order = new Order();
    //     medicalQuery.origin = req.body.origin;
    //     medicalQuery.opinionNose = req.body.opinionNose;
    //     medicalQuery.frontPicture = req.body.frontPicture;
    //     medicalQuery.basePicture = req.body.basePicture;
    //     medicalQuery.profilePicture = req.body.profilePicture;
    //     medicalQuery.customPicture = req.body.customPicture;
    //     medicalQuery.date = req.body.date;

    //     const dataMedicalQuery: Order = await mq.saveChanges(medicalQuery);

    //     const result = (dataMedicalQuery !== null) ? 'Datos actualizados con exito' : 'Ocurrio un error inesperado';
    //     res.status(200).json({ code: 200, response: result });
    //   } else {
    //     res.status(400).json({ code: 400, response: 'Fallo al actualizar los datos del paciente' });
    //   }
    // } catch (error) {
    //   res.status(500).json({ code: 500, response: error });
    // }
  }

  /**
   * Remove a patient on the database
   * @param req Request
   * @param res Response
   */
  public async ctrlRemove(req: Request, res: Response): Promise<void> {
    // try {
    //   // Search Doctor
    //   const or = new OrderModel();
    //   const query = await mq.getOneForDel(parseInt(req.params.id));
    //   if (query) {
    //     // Return data
    //     const data = await mq.remove(query.id);
    //     res.status(200).json({ code: 200, response: data });
    //   } else {
    //     res.status(404).json({ code: 404, response: 'Query not found' });
    //   }
    // } catch (error) {
    //   res.status(500).json({ code: 500, response: error });
    // }
  }
}

export const orderController = new OrderController();
