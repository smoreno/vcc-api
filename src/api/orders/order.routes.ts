import { Router } from 'express';
import { checkJwt } from '../../core/middlewares/check-jwt';
import { orderController } from './order.controller';

class OrderRoutes {
  public router: Router = Router();

  constructor() {
    this.config();
  }

  public config(): void {
    // Routes of Order
    this.router.get('/order', checkJwt, orderController.ctrlList);
    this.router.get('/:id([0-9]+)/order', checkJwt, orderController.ctrlGetList);
    this.router.post('/order', checkJwt, orderController.ctrlCreate);
    this.router.patch('/order', checkJwt, orderController.ctrlUpdate);
    this.router.delete('/order/:id([0-9]+)', checkJwt, orderController.ctrlRemove);

  }

}

export default new OrderRoutes().router;

