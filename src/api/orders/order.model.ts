import { DeleteResult, getManager } from 'typeorm';
import { Order } from '../../core/database/entities/order';

export class OrderModel {
  public async list(): Promise<Order[]> {
    return await getManager().getRepository(Order).find({
      relations: [ 'client', 'client.user' ],
    });
  }

  public async getOne(requestId: number): Promise<Order> {
    return await getManager().getRepository(Order).findOne({
      relations: [ 'client', 'client.user' ],
      where: { client: { id: requestId } },
    });
  }

  public async getOneByUpdate(requestId: number): Promise<Order> {
    return await getManager().getRepository(Order).findOne({
      where: { Clientid: requestId },
    });
  }

  public async getList(requestId: number): Promise<Order[]> {
    return await getManager().getRepository(Order).find({
      relations: [ 'client', 'client.user' ],
      where: { client: { id: requestId } },
    });
  }

  public async getOneForDel(orderId: number): Promise<Order> {
    return await getManager().getRepository(Order).findOne({
      where: { id: orderId },
    });
  }

  public async saveChanges(order: Order): Promise<Order> {
    return await getManager().getRepository(Order).save(order);
  }

  public async remove(orderId: number): Promise<DeleteResult> {
    return await getManager().getRepository(Order).delete(orderId);
  }
}
