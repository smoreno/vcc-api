import { DeleteResult, getManager } from 'typeorm';
import { User } from '../../core/database/entities/user';

export class AdministratorModel {
  public async list(): Promise<User[]> {
    return await getManager().getRepository(User).find({
      select: [ 'id', 'firstName', 'lastName', 'email' ],
      where: { role: 'Administrador' },
    });
  }

  public async getOne(administradorId: number): Promise<User> {
    return await getManager().getRepository(User).findOne({
      where: { id: administradorId, role: 'Administrador' },
    });
  }

  public async getOneWithoutRole(userId: number): Promise<User> {
    return await getManager().getRepository(User).findOne({
      where: { id: userId },
    });
  }

  public async saveChanges(user: User): Promise<User> {
    return await getManager().getRepository(User).save(user);
  }

  public async remove(administradorId: number): Promise<DeleteResult> {
    return await getManager().getRepository(User).delete(administradorId);
  }
}
