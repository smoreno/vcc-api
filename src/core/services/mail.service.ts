import nodemailer from 'nodemailer';
import { IMail } from '../interfaces/i-mail';

export class EmailService {
  private transport: any;
  private options: IMail;

  constructor() {
    this.defineTransporter();
  }

  public mailOptions(eTo: string, eSubject: string, eHtml: string): void {
    this.options = {
      from: '"Servicios Vcc Mexico" <proyectovccmx@gmail.com>',
      priority: 'high',
      encoding: 'utf-8',
      to: eTo,
      subject: eSubject,
      html: eHtml,
    };
  }

  public send(): void {
    this.transport.sendMail(this.options, (err: any, info: any) => {
      if (err) {
        console.log(err);
      } else {
        console.log('Email sent');
      }
    });
  }

  private defineTransporter(): void {
    this.transport = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: 'proyectovccmx@gmail.com',
        pass: 'proyectovccmx12!',
      },
    });
  }
}
