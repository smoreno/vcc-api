import { MaxLength } from 'class-validator';
import { Column, Entity, JoinColumn, OneToOne, PrimaryGeneratedColumn} from 'typeorm';
import { User } from './user';

// ENUMs
import Gender from '../helpers/gender';

@Entity()
export class Client {
  @PrimaryGeneratedColumn()
  public id: number;

  @OneToOne((type: any) => User, { onDelete: 'CASCADE' })
  @JoinColumn()
  public user: User;

  @Column()
  @MaxLength(20, { message: 'Ine is too long' })
  public ine: string;

  @Column({ type: 'enum', enum: Gender })
  public gender: string;

  @Column()
  @MaxLength(500, { message: 'Last Name is too long' })
  public address: string;

  @Column({ type: 'bigint' })
  public phone: number;
}
