import { Column, Entity, JoinColumn, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import DocumentTypeForPerson from '../helpers/document-type-of-person';
import Gender from '../helpers/gender';

// Entities
import { Subcategory } from './subcategory';
import { User } from './user';

@Entity()
export class Driver {
  @PrimaryGeneratedColumn()
  public id: number;

  @OneToOne((type: any) => User, { onDelete: 'CASCADE' })
  @JoinColumn()
  public user: User;

  @Column({ nullable: true })
  public license: string;

  @Column({ type: 'bigint' })
  public phone: number;
}
