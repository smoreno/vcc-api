import { MaxLength } from 'class-validator';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Subcategory } from './subcategory';

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column()
  public editable: boolean;

  @Column()
  @MaxLength(200, { message: 'Name is too long' })
  public name: string;

  @Column()
  @MaxLength(500, { message: 'Description is too long' })
  public description: string;

  @OneToMany((type: any) => Subcategory, (subcategory: Subcategory) => subcategory.category)
  public subcategories: Subcategory[];
}
