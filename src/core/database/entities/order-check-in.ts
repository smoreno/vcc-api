import { MaxLength } from 'class-validator';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

// ENTITIES
import { Driver } from './driver';
import { Order } from './order';

@Entity()
export class OrderCheckIn {
  @PrimaryGeneratedColumn()
  public id: number;

  @ManyToOne((type: any) => Driver, (driver: Driver) => driver.id, { onDelete: 'CASCADE' })
  public driver: Driver;

  @ManyToOne((type: any) => Order, (order: Order) => order.id, { onDelete: 'CASCADE' })
  public order: Order;

  @Column({ type: 'bigint' })
  public tipoGasolina: number;

  @MaxLength(100, { message: 'Value is too long' })
  public kilometraje: string;

  @Column({ type: 'bigint' })
  public manual: number;

  @Column({ type: 'bigint' })
  public tarjeta: number;

  @Column({ type: 'bigint' })
  public permiso: number;

  @Column({ type: 'bigint' })
  public talon: number;

  @Column({ type: 'bigint' })
  public carnet: number;

  @Column({ type: 'bigint' })
  public estuche: number;

  @Column({ type: 'bigint' })
  public nroLlave: number;

  @Column({ type: 'bigint' })
  public tapa: number;

  @Column({ type: 'bigint' })
  public gato: number;

  @Column({ type: 'bigint' })
  public llaveCruz: number;

  @Column({ type: 'bigint' })
  public kitInflado: number;

  @Column({ type: 'bigint' })
  public botiquin: number;

  @Column({ type: 'bigint' })
  public taponMotor: number;

  @Column({ type: 'bigint' })
  public herramienta: number;

  @Column({ type: 'bigint' })
  public compresor: number;

  @Column({ type: 'bigint' })
  public triangulo: number;

  @Column({ type: 'bigint' })
  public taponGas: number;

  @Column({ type: 'bigint' })
  public antena: number;

  @Column({ type: 'bigint' })
  public extintor: number;

  @Column({ type: 'bigint' })
  public encendedor: number;

  @Column({ type: 'bigint' })
  public portavasos: number;

  @Column({ type: 'bigint' })
  public radio: number;

  @Column({ type: 'bigint' })
  public cinturón: number;

  @MaxLength(30, { message: 'Value is too long' })
  public alfombra: string;

  @Column({ type: 'bigint' })
  public alzavidrio: number;

  @Column({ type: 'bigint' })
  public cerradura: number;

  @Column({ type: 'bigint' })
  public ac: number;

  @Column({ type: 'bigint' })
  public cierreCentral: number;

  @Column({ type: 'bigint' })
  public alarma: number;

  @Column({ type: 'bigint' })
  public gps: number;

  @Column({ type: 'bigint' })
  public sillaBebé: number;

  @Column({ type: 'bigint' })
  public luces: number;

  @Column({ type: 'bigint' })
  public aseoInt: number;

  @Column({ type: 'bigint' })
  public aseoExt: number;

  @Column({ type: 'bigint' })
  public retrovisor: number;

  @Column({ type: 'bigint' })
  public espejoLaterales: number;

  @MaxLength(100, { message: 'Value is too long' })
  public serialBateria: string;

  @Column({ type: 'bigint' })
  public estadoParabrisa: number;

  @MaxLength(100, { message: 'Value is too long' })
  public objetosDejado: string;

  @MaxLength(1000, { message: 'Value is too long' })
  public obsDerecho: string;

  @MaxLength(1000, { message: 'Value is too long' })
  public obsIzquierdo: string;

  @MaxLength(1000, { message: 'Value is too long' })
  public obsPosterior: string;

  @Column()
  public date: Date;
}
