import * as bcrypt from 'bcrypt';
import { IsEmail, MaxLength } from 'class-validator';
import { Column, Entity, ManyToOne, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Client } from './client';
import { Driver } from './driver';
import { Subcategory } from './subcategory';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  public id: number;
  @Column()
  @MaxLength(100, { message: 'Last Name is too long' })
  public orderNumber: string;

  @ManyToOne((type: any) => Client, (client: Client) => client.id, { onDelete: 'CASCADE' })
  public client: Client;

  @ManyToOne((type: any) => Driver, (driver: Driver) => driver.id, { onDelete: 'CASCADE' })
  public driver: Driver;

  @ManyToOne((type: any) => Subcategory, (subcategory: Subcategory) => subcategory.id, { onDelete: 'SET NULL' })
  public service: Subcategory;

  @ManyToOne((type: any) => Subcategory, (subcategory: Subcategory) => subcategory.id, { onDelete: 'SET NULL' })
  public state: Subcategory;

  @ManyToOne((type: any) => Subcategory, (subcategory: Subcategory) => subcategory.id, { onDelete: 'SET NULL' })
  public origin: Subcategory;

  @ManyToOne((type: any) => Subcategory, (subcategory: Subcategory) => subcategory.id, { onDelete: 'SET NULL' })
  public destination: Subcategory;

  @ManyToOne((type: any) => Subcategory, (subcategory: Subcategory) => subcategory.id, { onDelete: 'SET NULL' })
  public vehicle: Subcategory;

  @Column()
  public dateTransfer: Date;

  @Column()
  @MaxLength(500, { message: 'First Name is too long' })
  public manualVehicle: string;

  @Column()
  @MaxLength(500, { message: 'First Name is too long' })
  public observation: string;

  @Column()
  @MaxLength(100, { message: 'Last Name is too long' })
  public colour: string;

  @MaxLength(100, { message: 'Last Name is too long' })
  @Column({ nullable: true })
  public nameResponsable: string;

  @MaxLength(100, { message: 'Last Name is too long' })
  @Column({ nullable: true })
  public emailResponsable: string;

  @Column({ type: 'bigint' })
  public phoneResponsable: number;
}
