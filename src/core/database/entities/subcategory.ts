import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from './category';

@Entity()
export class Subcategory {
  @PrimaryGeneratedColumn()
  public id: number;

  @Column({ length: 200, nullable: false })
  public name: string;

  @ManyToOne((type: any) => Category, (category: Category) => category.subcategories, { onDelete: 'CASCADE' })
  public category: Category;
}
