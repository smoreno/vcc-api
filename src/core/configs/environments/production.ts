import { IEnvironment } from '../../interfaces/i-environment';

export const production: IEnvironment = {
  ENVIRONMENT: 'production',
  NODE_PORT: '3000',
  NODE_SERVER: 'https://froilan-api.herokuapp.com',
  CLIENT_SERVER: 'https://froilan-cli.herokuapp.com',
  DB_PORT: '3306',
  DB_HOST: 'us-cdbr-iron-east-05.cleardb.net',
  DB_NAME: 'heroku_b81a8eaab00adf6',
  DB_USER: 'b4c8dd1b3b0ebd',
  DB_PASSWORD: 'aa97cd16',
  DB_CHARSET: 'utf8_general_ci',
  DB_SYNCHRONIZE: true,
  DB_LOGGING: false,
};
