import { IEnvironment } from '../../interfaces/i-environment';

export const development: IEnvironment = {
  ENVIRONMENT: 'development',
  NODE_PORT: '3000',
  NODE_SERVER: 'https://dev-froilan-api.herokuapp.com',
  CLIENT_SERVER: 'https://dev-froilan.herokuapp.com',
  DB_PORT: '3306',
  DB_HOST: 'us-iron-auto-dca-02-b.cleardb.net',
  DB_NAME: 'heroku_5e736c35c94345a',
  DB_USER: 'bd3241ce589a18',
  DB_PASSWORD: 'c9e04ab3',
  DB_CHARSET: 'utf8_general_ci',
  DB_SYNCHRONIZE: true,
  DB_LOGGING: false,
};
